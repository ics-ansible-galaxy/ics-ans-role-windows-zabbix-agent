ics-ans-role-windows-zabbix-agent
===================

Ansible role to install windows-zabbix-agent.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
zabbix_agent_url: PATH_TO_ZABBIX-AGENT
zabbix_agent_ip: AGENT_IP_ADDRESS
zabbix_agent_port: '10050'
zabbix_agent_timeout: '10'
zabbix_server_ip: ZABBIX_SERVER_IPADDRESS
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-windows-zabbix-agent
```

License
-------

BSD 2-clause
